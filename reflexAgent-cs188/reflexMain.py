#!/usr/bin/python

"""
To check I2C address  for servo-16 is 0x40:
sudo i2cdetect -y 1
Reads servo config from pickles
"""

import math
from math import pi as PI
import time
import random
import logging

from reflexApp import *
from optparse import OptionParser

def setupLogger():
    logger = logging.getLogger("main") # base string for all messages
    logger.setLevel(logging.INFO)

    # create the logging file handler
    logFile="Log.reflexAgent"
    fh = logging.FileHandler(logFile) # physical log file
    print "Logger output contains precise timing:  tail -f ",logFile


    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    fh.setFormatter(formatter)

    # add handler to logger object
    logger.addHandler(fh)

    logger.info("reflex agent started -------\n")
    return 


#===============================
#=======   define   main_run()
#===============================
def main_run(): 

    tkWin = Tkinter.Tk()
    tkWin.title( 'Crawler  Reflex Agent GUI , jan.balewski@gmail.com' )
    tkWin.resizable( 0, 0 )

    # .... handle to controlls, it owns robot instance
    app = ApplicationCrawlerReflex(tkWin,options )

    print "====M: init completed ========"

    #..... function executed in an infinite loop by a different thread
    def update_gui():
        app.robotBody.draw(app.stepCount, app.tickTime) # draws robot
        tkWin.after(10, update_gui)

    #  ... controll self-loop activated   
    update_gui()

    #..... emergency termination of controll app
    tkWin.protocol( 'WM_DELETE_WINDOW', app.actExit)
    try:
        app.actStart()
    except:
        app.actExit()


#===============================
#=======    MAIN Program
#===============================

 
if __name__ == '__main__':
    
    # -- command line options
    usage = "usage: %prog [options]"
    parser = OptionParser(usage)
    
    parser.add_option("-m","--mockServos", 
                      dest="MOCK_SERVOS", 
                      action="store_true", 
                      help="disable servos & mouse position tracker", 
                      default = False)

    parser.add_option("-n","--name", 
                      dest="CONF_NAME", 
                      type="string",
                      help="crawler configuration file name (.conf)", 
                      default = "crawler5.conf")

    parser.add_option("-s","--setupPath", 
                      dest="SETUP", 
                      type="string",
                      help="hardware configuration location", 
                      default = "../setup/")

    parser.add_option("-v","--verbose", 
                      dest="VERBOSE", 
                      action = "store_true", 
                      help="print all variables", 
                      default = False)

 
    (options,args) = parser.parse_args()

    if options.VERBOSE:
        print ">>> Executing with parameters:"
        print ">>> CONF_NAME  = %s" % options.CONF_NAME 
        print ">>> setup PATH  = %s" % options.SETUP
        print ">>> SERVOS  = %s" % (not options.MOCK_SERVOS) 
 

 
    #################################
    #  MAIN
    #################################
    setupLogger()
    main_run()
