# servoCalibratorGUI
# ----------
# Calibrates standard servo : GUI
# Author: Jan Balewski (jan.balewski@gmail.com)
# Updated : July 2015

import time
import threading
import sys
import math
import os
import Tkinter
from math import pi as PI
import pickle


#--------------------------------------------
#------ define class GUIApplication:
#--------------------------------------------

class ApplicationGUI:

           
    # -----------------------------------   
    def setupIO_row(self,r0):
        c0=0
        Tkinter.Label(self.win, text='setup file').grid(row=r0, column=c0)
        
        self.confName=Tkinter.StringVar()
        xx= Tkinter.Entry(self.win, textvariable=self.confName,width=25).grid(row=r0, column=c0+1,columnspan=2)

        Tkinter.Button(self.win,text="write",command=(lambda: self.actConfig('write'))).grid(row=r0, column=c0+3)
        Tkinter.Button(self.win,text="read",command=(lambda: self.actConfig('read'))).grid(row=r0, column=c0+4)
        ## Exit Button ##
        Tkinter.Button(self.win,text='Quit', command=self.actExit, fg="blue",font=("Helvetica", 11)).grid(row=r0, column=c0+6)


    # -----------------------------------   
    def setupPwmLimits_row(self,r0):
        c0=0
        Tkinter.Label(self.win, text=' pwm limits', fg="red",font=("Helvetica", 11)).grid(row=r0, column=c0)
        
        Tkinter.Button(self.win,text="new Low :",command=(lambda: self.actLimit('low'))).grid(row=r0, column=c0+1)

        self.tk_lowLim = Tkinter.Label(self.win, text='fix99')
        self.tk_lowLim.grid(row=r0, column=c0+2)


        Tkinter.Button(self.win,text="new High :",command=(lambda: self.actLimit('high'))).grid(row=r0, column=c0+3)

        self.tk_highLim = Tkinter.Label(self.win, text='fix88')
        self.tk_highLim.grid(row=r0, column=c0+4)

        Tkinter.Button(self.win,text="full range",command=(lambda: self.actLimit('maximize'))).grid(row=r0, column=c0+5)

        

    def utilPwm2deg(self,pwm):
        return (  self.conf['pwmZero'] -1.*pwm  ) / self.conf['pwm2rad']/PI*180

    # -----------------------------------   
    def updatePwmLimits_row(self):

        angLow= self.utilPwm2deg(self.conf['pwmLow'])
        self.tk_lowLim['text']="%d =>%d deg    "%(self.conf['pwmLow'],int(angLow))
        angHigh= self.utilPwm2deg(self.conf['pwmHigh'])
        self.tk_highLim['text']="%d =>%d deg    "%(self.conf['pwmHigh'],int(angHigh))

    # -----------------------------------   
    def setupPwmZero_row(self,r0):
        c0=0
        Tkinter.Label(self.win, text=' pwmZero', fg="red",font=("Helvetica", 11)).grid(row=r0, column=c0)
        xx = Tkinter.Scale(self.win, from_=self.gui_pwmMin, to=self.gui_pwmMax,tickinterval=50, orient=Tkinter.HORIZONTAL,length=550,command=self.actMove2)
        xx.grid(row=r0,column=c0+1,columnspan=5, padx=10)
        xx.set(350)
        self.tk_pwmZero=xx
        r0+=1
        Tkinter.Label(self.win, text=' pwm/rad', fg="red",font=("Helvetica", 11)).grid(row=r0, column=c0)
        xx = Tkinter.Scale(self.win, from_= self.gui_radFac1, to= self.gui_radFac2,tickinterval=20, orient=Tkinter.HORIZONTAL,length=200,command=self.actMove2)
        xx.grid(row=r0,column=c0+1,columnspan=2, padx=10)
        xx.set(120)
        self.tk_pwm2rad=xx
        #
        self.var_inverted= Tkinter.IntVar()
        Tkinter.Checkbutton(self.win, text="Inverted", fg="red",font=("Helvetica", 11), variable=self.var_inverted,command=(lambda: self.actMove2('xx'))).grid(row=r0, column=c0+4)
        
        # 
        self.var_pwmChan= Tkinter.StringVar()
        Tkinter.Label(self.win, text='pwm chan:', fg="red",font=("Helvetica", 11)).grid(row=r0, column=c0+5)
        w =  Tkinter.Spinbox(self.win, from_=0, to=15,width=2,command=(lambda: self.actMove2('xx')),textvariable=self.var_pwmChan)
        w.grid(row=r0, column=c0+6)

        
    # -----------------------------------   
    def setupServo_row(self,r0):
        c0=0
    
        Tkinter.Label(self.win, text=' servo testing', fg="blue",font=("Helvetica", 11)).grid(row=r0, column=c0,columnspan=1)


        if self.useServos:
            self.actToggleServo(r0+2,c0)
            Tkinter.Label(self.win, text='DOWN <--  setAngle/deg   --> UP', fg="blue",font=("Helvetica", 11)).grid(row=r0+1, column=c0+4,columnspan=2)
            xx = Tkinter.Scale(self.win, from_=-90, to=30,tickinterval=30, orient=Tkinter.HORIZONTAL,length=250,command=self.actMove2)
            xx.grid(row=r0,column=c0+4,columnspan=2, padx=10)
            xx.set(0)
            self.tk_angle=xx
            xx=Tkinter.Label(self.win, text='pwm=x')
            xx.grid(row=r0, column=c0+2)
            self.tk_pwm=xx
        else:
            Tkinter.Label(self.win, text='absent').grid(row=r0, column=c0+1)


    #------------------------------------
    def actToggleServo(self,r0,c0):
        #print 'tag servo from', self.servoOn,r0,c0
        text0='Engaged'
        text1='Disable'
        if self.servoOn: #  value before toggle
            text0='Disabled'
            text1='Engage'
            self.servoDriver.disable()
            
        self.servoOn= not self.servoOn
        Tkinter.Label(self.win, text=text0).grid(row=r0, column=c0)
        Tkinter.Button(self.win, text = text1,command=(lambda: self.actToggleServo(r0,c0))).grid(row=r0, column=c0+1)
        



    # -----------------------------------           
    def __initGUI(self, win):
        ## Window ##
        self.win = win

        ## Initialize Frame ##
        win.grid()
        self.setupIO_row(1)
        self.setupPwmLimits_row(2) 
        self.setupPwmZero_row(3) 
        self.setupServo_row(5)


    # --------------------------       
    def __init__(self, win, opts):
        servoType = 'standardServo' # may be add it to pickel, later

        self.useServos=not opts.MOCK_SERVOS
        self.servoOn=False # 1st call will toggle it to on
        self.gui_pwmMin=150
        self.gui_pwmMax=650
        self.gui_radFac1=100
        self.gui_radFac2=160
 
        
        self.__initGUI(win) ## Init Gui

        self.confName.set(opts.SETUP+opts.CONF_NAME)
        self.actConfig('read')

        # Init hardware -if any
        if self.useServos:
            import servoDriver 
            if servoType ==  'standardServo':
                self.servoDriver = servoDriver.ServoDriver(1)
                self.servoDriver.setupController()
            else:
                raise "Unknown RobotType"
    # -----------------------------------            
    def actMove2(self,fake=0):
        self.conf['chan']=int(self.var_pwmChan.get())
        self.conf['pwmZero']=int(self.tk_pwmZero.get())
        self.conf['pwm2rad']=int(self.tk_pwm2rad.get())
        self.conf['inverted']=self.var_inverted.get()
        self.updatePwmLimits_row()

        if self.servoOn:
            self.servoDriver.config(self.conf)
            self.servoDriver.setAngleDeg(self.tk_angle.get())
            self.tk_pwm['text'] = 'pwm=%d   return:%s' % (self.servoDriver.pwmVal,self.servoDriver.status)

    #------------------------------------
    def actLimit(self,action):
        #print 'actLimit action=',action
        ival=self.conf['pwmZero']            
                  
        if action == 'low':            
            self.conf['pwmLow']=ival

        if action == 'high':            
            self.conf['pwmHigh']=ival

        if action == 'maximize':            
            self.conf['pwmLow']=self.gui_pwmMin
            self.conf['pwmHigh']=self.gui_pwmMax
        self.updatePwmLimits_row()

    #------------------------------------
    def actConfig(self,action):
        print 'actConfig action=',action
        pickleName=self.confName.get()+'.servo.conf'


        if action=='read':
            print 'reading pickle from :',pickleName
            if not os.path.isfile(pickleName) :
                print ' not existing input pickel name, do nothing ???'
                return
            self.conf=pickle.load( open(pickleName, "r" ) )
            self.var_pwmChan.set(self.conf['chan'])
            self.var_inverted.set(self.conf['inverted'])
            self.tk_pwmZero.set(int(self.conf['pwmZero']))
            self.tk_pwm2rad.set(int(self.conf['pwm2rad']))
            self.tk_angle.set(0) 

            print '  read  pwm0=',self.conf['pwmZero'],' chan=',self.conf['chan'], " ...etc"," inv=",self.conf['inverted']
            self.updatePwmLimits_row()
            
        if action=='write':
            print 'write pickle :',pickleName
            try:
                pickle.dump( self.conf, open(pickleName , "w" ) )
            except :
                print 'Error writing pickle:',pickleName,' continue...'

    def actExit(self):
        if self.useServos:
            self.servoDriver.fullStop() 
        try:
            self.win.destroy()
        except:
            pass
    
        if  self.useServos:
            self.useServos=False # to prevent multiple stops
            
        sys.exit(0)

 
    def actStart(self):
        self.win.mainloop()



 
