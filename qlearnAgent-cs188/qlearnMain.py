#!/usr/bin/python

# modified by Jan Balewski to controll physical crawler (jan.balewski@gmail.com)
#
import math
from math import pi as PI
import time
import random
from qlearnApp import *
from optparse import OptionParser

import sys
import os
# define path to all needed modules
sys.path.append(os.path.abspath("../utils-cs188"))
#print sys.path ;exit(0) # print path variable if you are in trouble

import logging

def setupLogger():
    """
    The main entry point of the application
    """
    logger = logging.getLogger("main") # base string for all messages
    logger.setLevel(logging.INFO)
 
    # create the logging file handler
    logFile="Log.qlearn"
    fh = logging.FileHandler(logFile) # physical log file
    print "Logger output contains precise timing:  tail -f ",logFile
 

    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    fh.setFormatter(formatter)
 
    # add handler to logger object
    logger.addHandler(fh)
 
    logger.info("qlearnMain  started -------\n")
    return logger


#===============================
#=======   define   main_run()
#===============================
def main_run():

    log=setupLogger()
    tkWin = Tkinter.Tk()
    tkWin.title( 'Crawler GUI w/ qlearn' )
    tkWin.resizable( 0, 0 )

    # .... handle to controlls, it owns robot instance
    print 'MAIN_RUN mock-servos=',options.MOCK_SERVOS, ' qvalGrid=',options.QGRID
    app = ApplicationCrawlerQlearn(tkWin,options)


    #.... 2nd canvas showing Q-values or state values
    if options.QGRID:
        from displayQvalGrid import DisplayQvalueGrid
        displayGrid = DisplayQvalueGrid(app.robotSM)
        app.showGrid=True
        try:
            displayGrid.start("Qvalues-crawler")
        except KeyboardInterrupt:
            sys.exit(0)

    print "====M: init completed ========"

    #..... function executed in an infinite loop by a different thread
    def update_gui():
        app.robotBody.draw(app.stepCount, app.tickTime) # draws robot
        if options.QGRID:
            displayGrid.displayQValues(app.learner, "Q-VAL "+str(app.stepCount)+" ITERATION")
        tkWin.after(10, update_gui)
    #  ... controll self-loop activated   
    update_gui()

    #..... emergency termination of controll app
    tkWin.protocol( 'WM_DELETE_WINDOW', app.actExit)
    try:
        app.actStart()
    except:
        app.actExit()


#===============================
#=======    MAIN Program
#===============================

 
if __name__ == '__main__':
    
    # -- command line options
    usage = "usage: %prog [options]"
    parser = OptionParser(usage)
    
    parser.add_option("-m","--mockServos", 
                      dest="MOCK_SERVOS", 
                      action="store_true", 
                      help="disable servos & mouse position tracker", 
                      default = False)

    parser.add_option("-n","--name", 
                      dest="CONF_NAME", 
                      type="string",
                      help="crawler configuration file name (.conf)", 
                      default = "crawler5.conf")

    parser.add_option("-s","--setupPath", 
                      dest="SETUP", 
                      type="string",
                      help="hardware configuration location", 
                      default = "../setup/")

      
    parser.add_option("-g","--grid", 
                      dest="QGRID", 
                      action="store_true", default = False,
                      help="enable display of Q-valu grid");
    
    parser.add_option("-v","--verbose", 
                      dest="VERBOSE", 
                      action = "store_true", 
                      help="print all variables", 
                      default = False)

    
    (options,args) = parser.parse_args()

  
    if options.VERBOSE:
        print ">>> Executing with parameters:"
        print ">>> CONF_NAME  = %s" % options.CONF_NAME 
        print ">>> setup PATH  = %s" % options.SETUP
        print ">>> SERVOS  = %s" % (not options.MOCK_SERVOS) 
        print ">>> Q-val GRID display = %s" % options.QGRID 
 

    #################################
    #  MAIN
    #################################

    main_run()
