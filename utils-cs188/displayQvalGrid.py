# former: graphicsGridworldDisplay.py
# ---------------------------
# Licensing Information:  You are free to use or extend these projects for 
# educational purposes provided that (1) you do not distribute or publish 
# solutions, (2) you retain this notice, and (3) you provide clear 
# attribution to UC Berkeley, including a link to 
# http://inst.eecs.berkeley.edu/~cs188/pacman/pacman.html
# 


import util
from graphicsUtils import *
class DisplayQvalueGrid:

    def __init__(self, SMworld, size=80, speed=1.0):
        self.SMworld = SMworld
        self.cellSize = size
        self.speed = speed

    def start(self, nameD="disp0"):
        setup(self.SMworld, size=self.cellSize, title =nameD)

    def pause(self):
        wait_for_keys()


    def displayQValues(self, agent, message = 'Agent Q-Values'):
        
        currentState = self.SMworld.getCurrentState()
        qValues = util.Counter()
        states = self.SMworld.getPossibleStates()
        for state in states:
            for action in self.SMworld.getPossibleActions(state):                
                qValues[(state, action)] = agent.getQValue(state, action)
                #if  state==(2,3):
                #    print "aa eval st rob=",state, " act=",action, " Qval=",qValues[(state, action)]
        drawQValues(self.SMworld, qValues, currentState, message)
        #sleep(0.05 / self.speed)

#---------------------------------------
# below are only global functions 
#---------------------------------------

#actionMap_crawler2gridwordld={'arm-up':'south','arm-down':'north',
#                              'hand-down':'west','hand-up':'east'}

actionMap_crawler2gridwordld={'hand-up':'north','hand-down':'south',
                              'arm-down':'west','arm-up':'east'}


        
BACKGROUND_COLOR = formatColor(0,0,0)
EDGE_COLOR = formatColor(1,1,1)
OBSTACLE_COLOR = formatColor(0.5,0.5,0.5)
TEXT_COLOR = formatColor(1,1,1)
MUTED_TEXT_COLOR = formatColor(0.7,0.7,0.7)
LOCATION_COLOR = formatColor(0,0,1)

WINDOW_SIZE = -1
GRID_SIZE = -1
GRID_HEIGHT = -1
MARGIN = -1

def setup(SMworld, title = "fixMe3-gridDisplay", size = 120):
    #return
    global GRID_SIZE, MARGIN, SCREEN_WIDTH, SCREEN_HEIGHT, GRID_HEIGHT
    grid = SMworld
    grid_height=SMworld.grid_height
    grid_width=SMworld.grid_width
    WINDOW_SIZE = size
    GRID_SIZE = size
    GRID_HEIGHT = grid_height     
    MARGIN = GRID_SIZE * 0.75
    screen_width = (grid_width - 1) * GRID_SIZE + MARGIN * 2
    screen_height = (grid_height - 0.5) * GRID_SIZE + MARGIN * 2

    print "setup-grid: title=",title,' size=',size, ' nY=',grid_height,' nX=',grid_width
    begin_graphics(screen_width,
                   screen_height,
                   BACKGROUND_COLOR, title=title)




#===============================================
#===============================================
def drawQValues(SMworld, qValues, currentState = None, message = 'State-Action Q-Values'):

    blank()

    #print 'drQv', ' nY=',SMworld.grid_height,' nX=',SMworld.grid_width
    stateCrossActions = [[(state, action) for action in SMworld.getPossibleActions(state)] for state in SMworld.getPossibleStates()]
    qStates = reduce(lambda x,y: x+y, stateCrossActions, [])
    qValueList = [qValues[(state, action)] for state, action in qStates] + [0.0]
    minValue = min(qValueList)
    maxValue = max(qValueList)

    for x in range(SMworld.grid_width):
        for y in range(SMworld.grid_height):
            state = (x,y)
            #print 'drQV state(x,y)=',state
            #j1 this section allows to stop on certain grid cells - disableit
            #j1 gridType = grid[x][y]
            #j1 isExit = (str(gridType) != gridType)
            isCurrent = (currentState == state)
            actions = SMworld.getPossibleActions(state)
            #print "posAct=",actions
            if actions == None or len(actions) == 0:
                actions = [None]
            bestQ = max([qValues[(state, action)] for action in actions])
            bestActions = [action for action in actions if qValues[(state, action)] == bestQ]

            q = util.Counter()
            valStrings = {}
            for action in actions:
                v = qValues[(state, action)]
                q[action] += v
                valStrings[action] = '%.1f' % v
                """
                #j1 another section is disabled   
            if gridType == '#':
                drawSquare(x, y, 0, 0, 0, None, None, True, False, isCurrent)
            elif isExit:
                action = 'exit'
                value = q[action]
                valString = '%.2f' % value
                drawSquare(x, y, value, minValue, maxValue, valString, action, False, isExit, isCurrent)
            else:
            """
            #print "A: bestAct=", state,bestActions
            drawSquareQ(x, y, q, minValue, maxValue, valStrings, bestActions, isCurrent)
    pos = to_screen(((SMworld.grid_width - 1.0) / 2.0, - 0.8))
    text( pos, TEXT_COLOR, message, "Courier", -32, "bold", "c")


#===============================================
#===============================================
def drawSquareQ(x, y, qVals, minVal, maxVal, valStrs, bestActions, isCurrent):

    (screen_x, screen_y) = to_screen((x, y))

    center = (screen_x, screen_y)
    nw = (screen_x-0.5*GRID_SIZE, screen_y-0.5*GRID_SIZE)
    ne = (screen_x+0.5*GRID_SIZE, screen_y-0.5*GRID_SIZE)
    se = (screen_x+0.5*GRID_SIZE, screen_y+0.5*GRID_SIZE)
    sw = (screen_x-0.5*GRID_SIZE, screen_y+0.5*GRID_SIZE)
    n = (screen_x, screen_y-0.5*GRID_SIZE+5)
    s = (screen_x, screen_y+0.5*GRID_SIZE-5)
    w = (screen_x-0.5*GRID_SIZE+5, screen_y)
    e = (screen_x+0.5*GRID_SIZE-5, screen_y)

    actions = qVals.keys()
    for actionRobot in actions:

        wedge_color = getColor(qVals[actionRobot], minVal, maxVal)
        actionGrid=actionMap_crawler2gridwordld[actionRobot]
        #print "x2y=",actionRobot,actionGrid

        if actionGrid == 'north':
            polygon( (center, nw, ne), wedge_color, filled = 1, smoothed = False)
         
        if actionGrid == 'south':
            polygon( (center, sw, se), wedge_color, filled = 1, smoothed = False)
           
        if actionGrid == 'east':
            polygon( (center, ne, se), wedge_color, filled = 1, smoothed = False)
            
        if actionGrid == 'west':
            polygon( (center, nw, sw), wedge_color, filled = 1, smoothed = False)
           

    square( (screen_x, screen_y),
                   0.5* GRID_SIZE,
                   color = EDGE_COLOR,
                   filled = 0,
                   width = 3)
    line(ne, sw, color = EDGE_COLOR)
    line(nw, se, color = EDGE_COLOR)

    if isCurrent:
        circle( (screen_x, screen_y), 0.1*GRID_SIZE, LOCATION_COLOR, fillColor=LOCATION_COLOR )

    for actionRobot in actions:
        text_color = TEXT_COLOR
        if actionRobot in  bestActions:
            text_color=formatColor(1,.5,0.2)
        if qVals[actionRobot] < max(qVals.values()): text_color = MUTED_TEXT_COLOR
        valStr = ""
        if actionRobot in valStrs:
            valStr = valStrs[actionRobot]
        h = -12
        actionGrid=actionMap_crawler2gridwordld[actionRobot]
        if actionGrid == 'north':
            text(n, text_color, valStr, "Courier", h, "bold", "n")
        if actionGrid == 'south':
            text(s, text_color, valStr, "Courier", h, "bold", "s")
        if actionGrid == 'east':
            text(e, text_color, valStr, "Courier", h, "bold", "e")
        if actionGrid == 'west':
            text(w, text_color, valStr, "Courier", h, "bold", "w")



#===============================================
def to_screen(point):
    ( gamex, gamey ) = point
    x = gamex*GRID_SIZE + MARGIN
    y = (GRID_HEIGHT - gamey - 1)*GRID_SIZE + MARGIN
    return ( x, y )


#===============================================
def blank():
    clear_screen()

#===============================================
def getColor(val, minVal, max):
    r, g = 0.0, 0.0
    if val < 0 and minVal < 0:
        r = val * 0.65 / minVal
    if val > 0 and max > 0:
        g = val * 0.65 / max
    return formatColor(r,g,0.0)


#===============================================
def square(pos, size, color, filled, width):
    x, y = pos
    dx, dy = size, size
    return polygon([(x - dx, y - dy), (x - dx, y + dy), (x + dx, y + dy), (x + dx, y - dy)], outlineColor=color, fillColor=color, filled=filled, width=width, smoothed=False)


#===============================================
