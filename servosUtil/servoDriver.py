# Author: Jan Balewski (jan.balewski@gmail.com)
# Updated : July 2015

"""
driver for standard servo , uses dictionary-based servo configuration
"""

# prevent use of this class on non-raspebbry pi hardware
import subprocess  # for checking if it runs on Rpi
# Get process info
unameL = subprocess.Popen(['uname','-a'], stdout=subprocess.PIPE).communicate()[0]
#print unameL
if 'raspberry' in unameL:
    print ' Raspberry-Pi hardware detected, proceed..'
else:
    print "Detected OS:\n\n%s\n is not Raspberry-Pi, the  CrawlerServo() class can't be invoked.\n Try add --mockServos option,  exiting\n" %unameL
    exit(1)


import os
import sys 

# define path to Adafruit library

sys.path.append(os.path.abspath("/home/pi/Adafruit-Raspberry-Pi-Python-Code/Adafruit_I2C"))
sys.path.append(os.path.abspath("/home/pi/Adafruit-Raspberry-Pi-Python-Code/Adafruit_PWM_Servo_Driver"))
#print sys.path ;exit(0) # print path variable if you are in trouble

from Adafruit_PWM_Servo_Driver import PWM

import time
import os
import math
from math import pi as PI

#==================================  
class ServoDriver():
    def __init__(self, dbg=0):

        if os.geteuid() != 0:
            exit("You need to have root privileges to run this script.\nPlease try again, this time using 'gksu'. Exiting.")

        self.dbg=dbg
         

    def setupController(self):
        pwm_freqHz=60 # no need to chang it ever
        print 'ServoDriver START:  freq=%d ' % pwm_freqHz
        # Initialise the PWM device using the default address
        self.pwmDriver= PWM(0x40, debug=False)
        self.pwmDriver.setPWMFreq(pwm_freqHz) 
        
        
    def linkController(self,pwmD):
        self.pwmDriver=pwmD

    def config(self,confDi):
        self.myPin=confDi['chan'] # assign pins on 16-servo controller    
        self.pwmZero=confDi['pwmZero'] 
        self.pwm2rad=confDi['pwm2rad'] 
        self.pwmLow=confDi['pwmLow'] 
        self.pwmHigh=confDi['pwmHigh'] 
        self.signFactor=1
        if confDi['inverted']:
            self.signFactor=-1
        #print 'servo-driver chan=',self.myPin
        self.pwmVal=self.pwmLow

    def rad2Pwm(self,ang):
        pwm=int (self.pwmZero + self.signFactor*ang*self.pwm2rad) # for plastic-gear servo from Parallax
        return pwm


    def setAngleDeg(self,angDeg):
        self.setAngle(angDeg/180.*PI)
        
    def setAngle(self,angRad):        
        newPwm=self.rad2Pwm(angRad)
        self.setPwm(newPwm)

    def setPwm(self,newPwm):     
        if newPwm < self.pwmLow:
            newPwm=self.pwmLow
            if self.signFactor>0:
                self.status='low'
            else : 
                self.status='high'

        elif newPwm > self.pwmHigh:
            newPwm=self.pwmHigh
            if self.signFactor>0:
                self.status='high'
            else:
                self.status='low'
        else:
            self.status='ok'
        
        self.pwmVal=newPwm
        self.pwmDriver.setPWM(self.myPin, 0, self.pwmVal)
        #print "serDr: new  pwm=%d, status=%s" % ( self.pwmVal,self.status)
        
        
    def disable(self):
        self.pwmDriver.setPWM(self.myPin, 0, 0)
        print 'disable chan=',self.myPin

    def fullStop(self):
        print "all Servos STOP & RELAX"
        self.pwmDriver.softwareReset()
  
