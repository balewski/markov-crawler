# god-class owning crawler body,SM,GUI,qlearn,qvalGrid,servos, mouse
# -------------------------
# Licensing Information:  You are free to use or extend these projects for 
# educational purposes provided that (1) you do not distribute or publish 
# solutions, (2) you retain this notice, and (3) you provide clear 
# attribution to UC Berkeley, including a link to 
# http://inst.eecs.berkeley.edu/~cs188/pacman/pacman.html
# 


import Tkinter
from qlearningAgents import QLearningAgent
import time
import threading
import sys,os

# define path to other modules
sys.path.append(os.path.relpath("../servosUtil/"))
sys.path.append(os.path.relpath("../reflexAgent-cs188/"))
sys.path.append(os.path.relpath("../mouseUtil/"))
#print sys.path ;exit(0) # print path variable if you are in trouble

import logging

from crawlerBodyModel import CrawlingRobotBody
from crawlerStateMachine import CrawlingRobotSM
    
import pickle
  
import math
from math import pi as PI

robotType = 'crawlerVirt'

#--------------------------------------------
#------ define class ApplicationCrawlerQlearn
#--------------------------------------------

class ApplicationCrawlerQlearn:

    def rewarder(self, move_rew,nextState):
        # experimental function to manipulate reward before update
        self.rew1=move_rew # memory of last reward
        if nextState==(0,0): # penalyze for the state it gets alwasy stuck
            return -10
        if move_rew>15:
            move_rew+=15
        return move_rew
    
    
    def sigmoid(self, x):
        sigmo=1.0 / (1.0 + 2.0 ** (-x))
        # print 'sigmo x=',x,' out=',sigmo
        return sigmo

    def incrementSpeed(self, inc):
        self.tickTime *= inc
        self.speed_label['text'] = 'Delay/sec: %.2g'  % (self.tickTime)

    def actPreSets(self):
        val=self.agentType.get()
        #print "preSets val=",val
        self.isVirtLearn=False
        self.mouse_off.set(0)
        if val==1: # virtual learn
            self.tickTime=0.05
            self.inc_eps=0
            self.inc_alp=2
            self.inc_gam=2
            self.isVirtLearn=True
            if self.showGrid:
                print 'WARN: virt-learning w/ gridi slower'
            if self.useServos:
                print 'WARN: virt-learning disables servos'
               
        elif val==10: # follow policy    
            self.tickTime=0.2
            self.inc_eps=-6
            self.inc_alp=-6
            self.inc_gam=10
            self.mouse_off.set(1)

        elif val==2: # phys learn Baby
            self.tickTime=0.3
            self.inc_eps=1
            self.inc_alp=2
            self.inc_gam=1

        elif val==3: # phys learn Child
            self.tickTime=0.2
            self.inc_eps=-1
            self.inc_alp=1  
            self.inc_gam=2

        elif val==4: # phys learn Adult
            self.tickTime=0.2
            self.inc_eps=-3
            self.inc_alp=0
            self.inc_gam=3


        if  self.useServos==False and val>= 2 and val<10:
                print 'WARN: physcial learning w/o servo is pointless'

        self.incrementSpeed(1)        
        self.incrementEpsilon(0)
        self.incrementAlpha(0)
        self.incrementGamma(0)

    def updateRewardMonitor(self,virt_rew, phys_pos, phys_rew):
        robPos = self.robotBody.getRobotPosition()
        self.rewMon_nstep['text'] =   'virt rew=%.0f' % virt_rew
        self.mouse_rew['text'] = 'mouse rew=%.0f' %float(phys_rew)
        self.mouse_pos['text'] = 'mouse pos=%.0f' % float(phys_pos)

    def incrementEpsilon(self, inc):
        self.inc_eps += inc
        self.epsilon = self.sigmoid(self.inc_eps)
        self.learner.setEpsilon(self.epsilon)
        self.epsilon_label['text'] = u'noise \u03b5  %.2f' % (self.epsilon)

    def incrementGamma(self, inc):
        self.inc_gam += inc
        self.gamma = self.sigmoid(self.inc_gam)
        self.learner.setDiscount(self.gamma)
        self.gamma_label['text'] = u'Discount \u03b3 %.2f' % (self.gamma)

    def incrementAlpha(self, inc):
        self.inc_alp += inc
        self.alpha = self.sigmoid(self.inc_alp)
        self.learner.setLearningRate(self.alpha)
        self.alpha_label['text'] = u'Learning \u03b1 %.2f' % (self.alpha)

    def __initGUI(self, win):
        ## Window ##
        self.win = win

        ## Initialize Frame ##
        win.grid()
        self.dec = -.5
        self.inc = .5
        self.tickTime = 0.9

        ## Epsilon Button + Label ##
        self.setupSpeedButtonAndLabel(win)

        self.setupEpsilonButtonAndLabel(win)

        ## Gamma Button + Label ##
        self.setUpGammaButtonAndLabel(win)

        ## Alpha Button + Label ##
        self.setupAlphaButtonAndLabel(win)


        ## Sepeed-dial buttons ##
        self.setupPreSetsButtonAndLabel()
        self.setupFormulas()

        ## reward monitoring block ##
        self.setupRewardMonitor()

        ## Exit Button ##
        self.exit_button = Tkinter.Button(win,text='Quit', command=self.actExit)
        self.exit_button.grid(row=0, column=9)

        ## Pause Button ##
        self.pause_button = Tkinter.Button(win,text='Pause', command=self.actPause)
        self.pause_button.grid(row=1, column=9)

        ## Resume Button ##
        self.resume_button = Tkinter.Button(win,text='Resume', command=self.actResume)
        self.resume_button.grid(row=1, column=8)

        ## OneStep Button ##
        self.onestep_button = Tkinter.Button(win,text='One Step', command=self.actStep)
        self.onestep_button.grid(row=0, column=8)

        ## write/read Qvalues Button ##
        self.exit_button = Tkinter.Button(win,text='writeQVal', command=self.actWriteQVal)
        self.exit_button.grid(row=0, column=7)
        self.exit_button = Tkinter.Button(win,text='readQVal', command=self.actReadQVal)
        self.exit_button.grid(row=1, column=7)

        ## Canvas ##
        self.canvas = Tkinter.Canvas(self.win, height=200, width=1100)
        self.canvas.grid(row=3,columnspan=10)

    def setupAlphaButtonAndLabel(self, win):
        self.alpha_minus = Tkinter.Button(win,
        text="-",command=(lambda: self.incrementAlpha(self.dec)))
        self.alpha_minus.grid(row=1, column=3, padx=10)

        self.alpha = self.sigmoid(self.inc_alp)
        self.alpha_label = Tkinter.Label(win, text=u'Learning \u03b1 %.2f' % (self.alpha))
        self.alpha_label.grid(row=1, column=4)

        self.alpha_plus = Tkinter.Button(win,
        text="+",command=(lambda: self.incrementAlpha(self.inc)))
        self.alpha_plus.grid(row=1, column=5, padx=10)

    def setUpGammaButtonAndLabel(self, win):
        self.gamma_minus = Tkinter.Button(win,
        text="-",command=(lambda: self.incrementGamma(self.dec)))
        self.gamma_minus.grid(row=0, column=3, padx=10)
        self.gamma = self.sigmoid(self.inc_gam)
        self.gamma_label = Tkinter.Label(win, text=u'Discount \u03b3 %.2f' % (self.gamma))
        self.gamma_label.grid(row=0, column=4)
        self.gamma_plus = Tkinter.Button(win,
        text="+",command=(lambda: self.incrementGamma(self.inc)))
        self.gamma_plus.grid(row=0, column=5, padx=10)

    def setupEpsilonButtonAndLabel(self, win):
        self.epsilon_minus = Tkinter.Button(win,
        text="-",command=(lambda: self.incrementEpsilon(self.dec)))
        self.epsilon_minus.grid(row=1, column=0)

        self.epsilon = self.sigmoid(self.inc_eps)
        self.epsilon_label = Tkinter.Label(win, text=u'noise \u03b5 %.2f' % (self.epsilon))
        self.epsilon_label.grid(row=1, column=1)

        self.epsilon_plus = Tkinter.Button(win,
        text="+",command=(lambda: self.incrementEpsilon(self.inc)))
        self.epsilon_plus.grid(row=1, column=2)


    def setupSpeedButtonAndLabel(self,win):
        self.speed_minus = Tkinter.Button(win,
        text="-",command=(lambda: self.incrementSpeed(.5)))
        self.speed_minus.grid(row=0, column=0)
        self.speed_label = Tkinter.Label(win, text='Delay/sec %.2g' % (self.tickTime))
        self.speed_label.grid(row=0, column=1)
        self.speed_plus = Tkinter.Button(win,
        text="+",command=(lambda: self.incrementSpeed(2)))
        self.speed_plus.grid(row=0, column=2)


    def setupRewardMonitor(self):
        col=0; row=5;
        self.rewMon_nstep=Tkinter.Label(self.win, text='virt rew=?')
        self.rewMon_nstep.grid(row=row, column=col+4)
        self.mouse_rew = Tkinter.Label(self.win, text='mou rew=none ')
        self.mouse_rew.grid(row=row, column=col+3)
        self.mouse_pos = Tkinter.Label(self.win, text='mou pos=none ')
        self.mouse_pos.grid(row=row, column=col+2)

        self.mouse_off= Tkinter.IntVar()
        Tkinter.Checkbutton(self.win, text="disable mouse", variable=self.mouse_off).grid(row=row, column=col+6,columnspan=2)

    def setupFormulas(self):
        col=0; row=2;
        Tkinter.Label(self.win, text=u' Value iter.:   V(s) \u2190 (1-\u03B1)*V(s) + \u03B1*[ R(s,\u03c0(s),s\') + \u03b3*V(s\') ]')\
            .grid(row=row, column=col,columnspan=3)
        Tkinter.Label(self.win, text=u' Action:   Pr( a=\u03c0(s) )=1-\u03b5   ,  Pr( a=random )=1-\u03b5 ')\
            .grid(row=row, column=col+4,columnspan=3)


    def setupPreSetsButtonAndLabel(self):
        col=0; row=6;
        Tkinter.Label(self.win, text='LEARN:').grid(row=row, column=col)
        self.agentType =  Tkinter.IntVar()
        self.agentType.set(1)

        Tkinter.Radiobutton(self.win, text="virtual", variable=self.agentType, value=1, command=self.actPreSets).grid(row=row, column=col+1, padx=10)
        Tkinter.Radiobutton(self.win, text="phys BABY", variable=self.agentType, value=2, command=self.actPreSets).grid(row=row, column=col+2, padx=10) 
        Tkinter.Radiobutton(self.win, text="phys CHILD", variable=self.agentType, value=3, command=self.actPreSets).grid(row=row, column=col+3, padx=10) 
        Tkinter.Radiobutton(self.win, text="phys ADULT", variable=self.agentType, value=4, command=self.actPreSets).grid(row=row, column=col+4, padx=10) 
        Tkinter.Radiobutton(self.win, text=u"Follow policy \u03c0(s)", variable=self.agentType, value=10, command=self.actPreSets).grid(row=row, column=col+6, padx=10) 



    def __init__(self, win, opts):

        self.dbg=opts.VERBOSE
        self.showGrid=False
        self.bodyConfigFile=opts.SETUP+opts.CONF_NAME
        self.policyFile=opts.SETUP+'q-policy.one'
        execfile(self.bodyConfigFile) # load  physical specs of the crawler
   
        if self.dbg:
            print "Define mechanical crawler body using:",bodyConfigFile
            print crawlerConfig

        # those are increments for respective values if +/-button is pressed
        self.inc_eps = 0
        self.inc_gam = 2
        self.inc_alp = 2
        self.stepCount = 0
        self.currentSleep=0.01 # some working variable
        self.rew1=0 # experimenting with multi-step rewards

        logger = logging.getLogger("main.app")
        logger.setLevel(logging.INFO)
        logger.info("starting %s"%robotType)

        ## Init Gui
        self.__initGUI(win)

        # Init environment
        if robotType == 'crawlerVirt':
            self.robotBody = CrawlingRobotBody(self.canvas, crawlerConfig)
            self.robotSM = CrawlingRobotSM(self.robotBody, crawlerConfig)
        else:
            raise "Unknown RobotType"

        ## Robot Servos - if activated##
        if not opts.MOCK_SERVOS:
            from crawlerServos import CrawlerServos
            from mouseDriver import MouseDriver 
            print 'Connecting to servos ....'
            self.useServos=True
            self.servos=CrawlerServos(crawlerConfig, opts.SETUP, self.dbg)
            print 'Connecting to  mouse....'           
            pickleName=opts.SETUP+crawlerConfig['mouseConfig']
            print "Load mouse config from ",pickleName 
            if not os.path.isfile(pickleName) :
                print ' not existing input pickel name, do nothing ???'
                exit(1)
            mConf=pickle.load( open(pickleName, "r" ) )
            self.mouse=MouseDriver(mConf)
            self.mouse.start()  # start a thread for mouse 
            logger.info("set mousePix2mm %s"%mConf['mousePix2mm'])
        else:
            self.useServos=False
            print 'NOT connecting to hardware '


        # Init Agent

        actionFn = lambda state: \
          self.robotSM.getPossibleActions(state)
        self.learner = QLearningAgent(actionFn=actionFn)

        self.learner.setEpsilon(self.epsilon)
        self.learner.setLearningRate(self.alpha)
        self.learner.setDiscount(self.gamma)

        # Start GUI
        self.running = False
        self.stopped = False
        self.stepsToSkip = 0
        self.actPreSets() 
    
    def actPause(self):
        self.running = False

    def actWriteQVal(self):
        print "write QValues to  file=",self.policyFile
        self.running = False
        self.learner.qvalues.saveAsPickel(self.policyFile)
  
    def actReadQVal(self):
        self.running = False
        print "read QValues from  file=",self.policyFile
        self.learner.qvalues.readFromPickel(self.policyFile)
      
    def actResume(self):
        self.running = True
        self.thread = threading.Thread(target=self.actRun)
        self.thread.start()


    def actExit(self):
        self.running = False
        for i in range(5):
            if not self.stopped:
                time.sleep(0.1)
        try:
            self.win.destroy()
        except:
            pass
        sys.exit(0)

    def actStep(self):
        # here the delay must be build-in, so serve have time to act
        self.stepCount += 1
        logger = logging.getLogger("main.app.actStep")
        state = self.robotSM.getCurrentState()
        actions = self.robotSM.getPossibleActions(state)
        if len(actions) == 0.0:
            self.robotEnvironment.reset()
            state = self.robotEnvironment.getCurrentState()
            actions = self.robotEnvironment.getPossibleActions(state)
            print 'Reset! BAD 1  should never happen'
            exit(1)
        action = self.learner.getAction(state)
        if action == None:
            raise 'None action returned: Code Not Complete'
        # brain makes decision what to do
        nextState, virt_rew, virt_pos = self.robotSM.doAction(action)
               
        if self.useServos and not self.isVirtLearn:
            armAng,handAng=self.robotBody.getAngles()
            logger.info("%d step BEGIN action=%s"%(self.stepCount,action) )
            self.servos.setSoftly( armAng, handAng-armAng,self.tickTime)
            logger.info("%d step servo moved"%self.stepCount )
        else:
            time.sleep(self.tickTime)
        phys_rew=-999
        phys_pos=-999

        if self.useServos and  self.mouse_off.get():
            logger.info("%d step ignore mouse"%self.stepCount )
            self.updateRewardMonitor(virt_rew, phys_pos, phys_rew)
            return

        if self.useServos and not self.isVirtLearn:
            logger.info("%d step asking mouse"%self.stepCount )
            phys_rew, phys_pos,phys_dur=self.mouse.getRestReward()            
            logger.info("%d step mouse rew=%.1f pos=%.1f ; virt rew=%.1f pos=%.1f"%(self.stepCount, phys_rew, phys_pos,virt_rew, virt_pos) )
            move_rew=phys_rew
        else:
            move_rew=virt_rew

        reward=self.rewarder(move_rew,nextState)    

        #print "actStep rewards: virt=",virt_rew, ' mouse=', mou_rew," used:",reward   
        self.learner.observeTransition(state, action, nextState, reward)
        self.updateRewardMonitor(virt_rew, phys_pos, phys_rew)

 
    def actRun(self):
        self.stepCount = 0
        par_reset_pos=90
        self.robotBody.reset(par_reset_pos)
        if self.useServos:
            self.mouse.posReset(par_reset_pos)
        self.learner.startEpisode()
        while True:
            self.actStep()
            minSleep = .01
            self.currentSleep = max(minSleep, self.tickTime)            
            self.stepsToSkip = int( self.currentSleep / self.tickTime) - 1
            #print 'r: currSleep=', self.currentSleep, 'stSk=',self.stepsToSkip 

            if  self.stepsToSkip >0:
                if  self.useServos and not self.isVirtLearn:
                    print 'WARN requested too high speed w/ servos, abort'
                    raise 'Operator error 1'
                time.sleep( self.currentSleep)

            if not self.running:
                self.stopped = True
                return
            for i in range(self.stepsToSkip):
                self.actStep()
            self.stepsToSkip = 0

        self.learner.stopEpisode()

    def actStart(self):
        self.win.mainloop()


