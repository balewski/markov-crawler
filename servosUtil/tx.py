#!/usr/bin/python
# Author: Jan Balewski (jan.balewski@gmail.com)
# Updated July 2015

import time
import os
import pickle
from servoDriver import ServoDriver 

confName ="../setup/hand.servo.conf"    
#confName ="../setup/arm.servo.conf"    

conf=pickle.load( open(confName, "r" ) )

servo = ServoDriver()
servo.setupController(conf['freq'])
servo.config(conf)

for ang in [ 0., 30., -30, 0.]:
    servo.setAngleDeg(ang)
    print 'set angle(deg)=%s, status=%s '%(ang,servo.status)
    time.sleep(0.8) # important to give servo some time to reach new position
    
servo.fullStop()  # now A-meter will show no current 
