#!/usr/bin/python
# Author: Jan Balewski (jan.balewski@gmail.com)
# Updated July 2015

"""
 Example of using calibrated hand-servo
"""

import time, os

from servoDriver import ServoDriver 
import pickle

#===============================
#=======    MAIN Program
#===============================

confName ="../setup/hand.servo.conf"    
#confName ="../setup/arm.servo.conf"    

print 'reading pickle from :',confName
if not os.path.isfile(confName) :
    print ' not existing input pickel name, fix it, aborting'
    exit(1)
 

conf=pickle.load( open(confName, "r" ) )

servo = ServoDriver()
servo.setupController()
servo.config(conf)

print "Execute users sequence of angles:"
for ang in [15.,  -60, 30., 0.]:
    servo.setAngleDeg(ang)
    print 'set AngDeg=%s, status=%s '%(ang,servo.status)
    time.sleep(1.1) # important to give servo some time to reach new position
    
servo.fullStop()  # now A-meter will show no current 
