"""
the thread which monitors the movement of the mouse
"""

import threading
import time
import logging

#==================================  
class MouseDriver(threading.Thread): 
    def __init__(self,mConf):
        threading.Thread.__init__(self)
        self.mmpos = 0
        self.Rbut=0
        self.Lbut=0
        self.needToStop = False
        self.par_pix2mm=mConf['mousePix2mm']
        self.par_devName=mConf['mouseDevice']
        self.par_moveDir=mConf['mouseMoveDir']
        self.last_mmpos=self.mmpos
        self.last_time= time.time()
        print 'mouse cnstr pix2mm=', self.par_pix2mm,' moveDir=',self.par_moveDir
        logger = logging.getLogger("main.mouse.init")
        logger.info("pix2mm=%.1f dir=%d"%(self.par_pix2mm, self.par_moveDir))

 
    #---------------------
    def getStepReward(self): 
        # returns reward position

        logger = logging.getLogger("main.mouse.stepRew")
        logger.info("new pos=%d dir=%d"%(self.mmpos, self.par_moveDir))

        #print 'new pos=',self.mmpos,' last=',self.last_mmpos
        new_mmpos=self.mmpos
        reward=new_mmpos - self.last_mmpos
        self.last_mmpos=new_mmpos
        return  reward, new_mmpos

    #---------------------
    def getRestReward(self,restTime=0.15): # wait at least that long to verify mouse is not moving
        new_mmpos=self.mmpos
        nCheck=0
        while True:
            time.sleep(restTime)
            nCheck=nCheck+1
            if new_mmpos==self.mmpos:
                break
            new_mmpos=self.mmpos
        logger = logging.getLogger("main.mouse.restRew")
        time1 = time.time()
        elapsedT=time1-self.last_time
        self.last_time=time1
        reward=new_mmpos - self.last_mmpos
        self.last_mmpos=new_mmpos

        logger.info("reward/mm=%.1f elapsedT/sec=%.3f nCheck=%d"%(reward,elapsedT,nCheck))

        return  reward, new_mmpos,elapsedT

    #------------
    def posReset(self,val=0):
         self.last_mmpos=val
         self.mmpos=val

    def stop(self):
        print "mouseDriver-stop, move mouse to realy exit program "
        self.kill_received = True
        self.needToStop=True

    def run(self):
        self.f = open(self.par_devName, 'rbw')
 
        print "mouse-thread started,  pix2mm=",self.par_pix2mm
        while (not self.needToStop):
 
            # read from the file. ord() converts char to integer
            state = ord(self.f.read(1))
            dx = ord(self.f.read(1))
            dy = ord(self.f.read(1))
 
            #convert bits in 'state' in to an array 'mouse_state'
            mouse_state = [(state & (1 << i)) >> i for i in xrange(8)]
 
            # if mouse moving to the left. dx must be a negative value
            if mouse_state[4] == 1:
                    dx = dx - 256    # convert 2's complement negative value
 
            # if mouse moving down
            if mouse_state[5] == 1:
                    dy = dy - 256
 
            if (mouse_state[6] == 1) or (mouse_state[7]==1):
                    print "Overflow!"
 
            # update the position & buttons
 
            if self.par_moveDir==1:        
                self.mmpos += dx /self.par_pix2mm
            else:
                self.mmpos += dy /self.par_pix2mm
            self.Lbut=mouse_state[0]
            self.Rbut=mouse_state[1]
 

        print "Exiting optical-mouse thread"

