#!/usr/bin/python

"""
Example of reflex agent driving 2 servos using a list of pair of angles
Walking is very abrupt with jerks. Servo  move abruptly by large angles.

Main must be executed as sudo or gksudo
"""

import sys
import os
# define path to other modules
sys.path.append(os.path.relpath("../servosUtil/"))
#print sys.path ;exit(0) # print path variable if you are in trouble

from servoDriver import ServoDriver 

import time
import pickle

#===============================
#=======    MAIN Program
#===============================


# define parameters below
walkTime=10. #  (sec) , total duration of walking
stepDelay=0.3 # (seconds), between steps
handConfName="../setup/hand.servo.conf"
armConfName ="../setup/arm.servo.conf"
dbg=0  # debugging switch , set to 1 to see more printouts

# open pickles for hand and arm and initialize two instances of ServoDriver
handConf=pickle.load( open(handConfName, "r" ) )
print 'loaded hand-servo conf',handConfName
if dbg:
    print handConf

hand = ServoDriver(dbg)
hand.setupController()
hand.config(handConf)

armConf=pickle.load( open(armConfName, "r" ) )
print 'loaded arm-servo conf ',armConfName
if dbg:
    print armConf
    
arm = ServoDriver(dbg)
arm.linkController(hand.pwmDriver) # use commin PWM driver
arm.config(armConf)

"""
position arm,hand  in some neutral angle, 
must execute sequential because at cold-start the servos positions are unknown
and they can draw a lot of current
"""

stateReset=(20,-15) # (arm,hand) angles in deg  at reset position
arm.setAngleDeg(stateReset[0])
time.sleep(0.5) # this delay desynchronizes hand from arm, keep it 
hand.setAngleDeg(stateReset[1])
time.sleep(0.5)
print "Arm and Hand ready"

# definition of (arm,hand) relative angular states in deg for one cycle
statList=[stateReset,(-5,-45),(-20,-80),(-40,-100),(-5,-160),(30,-150)]


while walkTime >0 :
    print "-----walking time left %.1f (sec) "%walkTime

    for state in statList:
        arm.setAngleDeg(state[0])
        hand.setAngleDeg(state[1])
        print "goal state(deg)=",state," response arm=%s hand=%s"%(arm.status,hand.status)
        time.sleep(stepDelay)
        walkTime=walkTime - stepDelay

    
hand.fullStop()  # must be executed to free servos

