#!/usr/bin/python
"""
 turns off PWM generator and sorvos relax immediately
"""

import sys
import os
# define path to other modules
sys.path.append(os.path.relpath("../servosUtil/"))
#print sys.path ;exit(0) # print path variable if you are in trouble

from servoDriver import ServoDriver 


driver = ServoDriver(0)
driver.setupController()
driver.fullStop()  # must be executed to free servos

