#!/usr/bin/python
"""
Example of reflex agent driving 2 servos using a varying in time functions.
Walking is smooth, periodically the direction is reversed by adding 180 deg
to the relative phase of angle-arm.

Main must be executed as sudo or gksudo

"""

import sys
import os
import time
import pickle
import math

# define path to other modules
sys.path.append(os.path.relpath("../servosUtil/"))
#print sys.path ;exit(0) # print path variable if you are in trouble

from servoDriver import ServoDriver 
import sys

print 'Number of arguments:', len(sys.argv), 'arguments.'
print 'Argument List:', str(sys.argv)

#===============================
#=======    MAIN Program
#===============================

# define parameters below
maxReverse=4  # how many times the walking direction will be reversed
walkTime=10. #  (sec) , total duration of walking in one direction
stepDelay=0.05 # (seconds), between steps
rotation_omega=250  # deg/second

# define  mean angle and amplitude of oscillations, units: deg
arm_phi0=-10
arm_ampl=30
hand_phi0=-100
hand_ampl=45


handConfName="../setup/hand.servo.conf"
armConfName ="../setup/arm.servo.conf"
dbg=0  # debugging switch , set to 1 to see more printouts

# open pickles for hand and arm and initialize two instances of ServoDriver
handConf=pickle.load( open(handConfName, "r" ) )
print 'loaded hand-servo conf',handConfName
if dbg:
    print handConf

hand = ServoDriver(dbg)
hand.setupController()
hand.config(handConf)

armConf=pickle.load( open(armConfName, "r" ) )
print 'loaded arm-servo conf ',armConfName
if dbg:
    print armConf
    
arm = ServoDriver(dbg)
arm.linkController(hand.pwmDriver) # use commin PWM driver
arm.config(armConf)

"""
position arm,hand  in some neutral angle, 
must execute sequential because at cold-start the servos positions are unknown
and they can draw a lot of current
"""

stateReset=(20,-15) # (arm,hand) angles in deg  at reset position
arm.setAngleDeg(stateReset[0])
time.sleep(0.5) # this delay desynchronizes hand from arm, keep it 
hand.setAngleDeg(stateReset[1])
time.sleep(0.5)

print "Arm and Hand ready"

cycleLength=walkTime
arm_hand_phase=80  #start forward
while maxReverse>0 :
    while walkTime >0 :
    
        x=rotation_omega*walkTime
        xHand=math.radians(x)
        xArm=math.radians(x-arm_hand_phase)
        hand_phi= hand_phi0+math.sin(xHand)*hand_ampl
        arm_phi= arm_phi0+math.sin(xArm)*hand_ampl

        arm.setAngleDeg(arm_phi)
        hand.setAngleDeg(hand_phi-arm_phi)
        time.sleep(stepDelay)
        walkTime=walkTime - stepDelay
    print "inverse direction, maxReverse=",maxReverse    
    maxReverse=maxReverse-1
    arm_hand_phase=arm_hand_phase+180
    walkTime=cycleLength
    time.sleep(1.0)     


arm.setAngleDeg(stateReset[0])
hand.setAngleDeg(stateReset[1])
time.sleep(1.0)    
hand.fullStop()  # must be executed to free servos

