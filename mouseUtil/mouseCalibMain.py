#!/usr/bin/python

# ----------
# Calibrates mouse 
# input  : run time params
# output : pickle
# Author: Jan Balewski (jan.balewski@gmail.com)
# Updated October 2015

import time
import pickle
import logging
from optparse import OptionParser
from mouseDriver import MouseDriver

def setupLogger():
    logger = logging.getLogger("main") # base string for all messages
    logger.setLevel(logging.INFO)

    # create the logging file handler
    logFile="Log.mouseCalib"
    fh = logging.FileHandler(logFile) # physical log file
    print "Logger output contains precise timing:  tail -f ",logFile


    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    fh.setFormatter(formatter)

    # add handler to logger object
    logger.addHandler(fh)

    logger.info("mouse calibration started -------\n")
    return 


#===============================
#=======    MAIN Program
#===============================

if __name__ == '__main__':
    
    # -- command line options
    usage = 'usage: %prog [options]'
    parser = OptionParser(usage)
    
    
  
    parser.add_option("-m","--mouseDevice",
                      dest="MOUSE_DEV",
                      type="string",
                      help="mouse device", 
                      default = "/dev/input/mouse0")

  
    parser.add_option("-c","--convFact",
                      dest="PIX2MM",
                      type="float",
                      help="pixel to mm conversion factor", 
                      default = 26.)


    parser.add_option("-d","--moveDir",
                      dest="MOVE_DIR",
                      type="int",
                      help="moving direction 1 or 2",
                      default = 1)


    parser.add_option("-n","--name", 
                      dest="NAME", 
                      type="string",
                      help="mouse setup (pickle) name", 
                      default = "mouse")

    parser.add_option("-s","--setupPath", 
                      dest="SETUP", 
                      type="string",
                      help="hardware configuration location", 
                      default = "../setup/")

    (opt,args) = parser.parse_args()
  


    print ">>> Executing with parameters:"
    print " mouse dev=%s  scaleFactor=%f"%(opt.MOUSE_DEV,opt.PIX2MM)
    pickleName=opt.SETUP+"/"+opt.NAME+".conf"
    print " target pickle  name:",pickleName
   
    #################################
    #  MAIN
    #################################

    setupLogger()
 
    # prepare config (dictionary)
    mConf={}
    mConf['mouseDevice']=opt.MOUSE_DEV
    mConf['mouseMoveDir']=opt.MOVE_DIR
    mConf['mousePix2mm']=opt.PIX2MM
    
    mouse=MouseDriver(mConf)
    mouse.start()  # start the thread1 for mouse
    time.sleep(0.5)
  

    print "press: ENTER for distance or, s (save pickel) , q (quite w/o saving) t (timed reward):",
    while 1:
        inkey = raw_input()

        if inkey == "":
            #print "got1:",inkey
            phys_rew, phys_pos=mouse.getStepReward()
            print " mouse distance(mm) step=%.1f  integral=%.1f"%(phys_rew, phys_pos)
        elif inkey == "s":
            try:
                pickle.dump( mConf, open(pickleName , "w" ) )
            except :
                print 'Error writing pickle:',pickleName
                break
            print "saved pickel:",pickleName
            break
                
        elif inkey == "q":
            #print "got2:",inkey," and quit"
            break
        elif inkey == "t":
            restTime=2.
            print "will do timed reward, restTime/sec=",restTime
            while 1:
                phys_rew, phys_pos, phys_duration=mouse.getRestReward(restTime)
                print "step dist/mm=%.1f duration/sec=%.2f totalPath/mm=%.1f"%(phys_rew, phys_duration,phys_pos)
            break
        else:
            print "Other input:",inkey," try again:",
        print "press:",    
    mouse.stop()  # stop the thread1 for mouse
    print "Out of loop"
 



