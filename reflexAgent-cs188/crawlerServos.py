"""
main must be executed as sudo or gksudo

operates to regular servos connected as arm and hand
intends to move crawler form my AI class

"""

from servoDriver import ServoDriver 
import os
import time

import math
import pickle

#==================================  
class CrawlerServos():
    def __init__(self, crawlerConfig,  inpPath, dbg=0):
       
        handConfName=inpPath+crawlerConfig['handServoConfig']
        armConfName =inpPath+crawlerConfig['armServoConfig']
        self.tickSec=0.05 # servo micro-steps

        self.dbg=dbg

        handConf=pickle.load( open(handConfName, "r" ) )
        print 'loaded hand-servo conf',handConfName
        if self.dbg:
            print handConf

        self.hand = ServoDriver(dbg)
        self.hand.setupController()
        self.hand.config(handConf)


        armConf=pickle.load( open(armConfName, "r" ) )
        print 'loaded arm-servo conf ',armConfName
        if dbg:
            print armConf

        self.arm = ServoDriver(dbg)
        self.arm.linkController(self.hand.pwmDriver)
        self.arm.config(armConf)

        # position arms in some neutral angle, order may matter
        self.setHardArm(0.5) # init angle/rad
        self.setHardHand(-0.9) # init angle/rad        


    def setHardArm(self,angRad,timeSec=1.):
        print 'setHard armAngRad=%.3f , pause for timeSec=%.2f'%(angRad,timeSec)
        self.arm.setAngle(angRad)
        time.sleep(timeSec)

    def setHardHand(self,angRad,timeSec=1.):
        print 'setHard armAngRad=%.3f , pause for timeSec=%.2f'%(angRad,timeSec)
        self.hand.setAngle(angRad)
        time.sleep(timeSec)

    def setSoftly(self,armAngRad,handAngRad,timeSec):
        if self.dbg:
            print 'sf: old pwm: arm=%d hand=%d' %  (self.arm.pwmVal, self.hand.pwmVal)        
            print 'setSoftly2 armAngRad=%.3f ,handAngRad=%3f ,timeSec=%.2f tickSec=%.3f'%(armAngRad,handAngRad,timeSec,self.tickSec)
        
        armPwm=self.arm.rad2Pwm(armAngRad)
        handPwm=self.hand.rad2Pwm(handAngRad)
        if self.dbg:
            print 'new set pwm arm=%d hand=%d' %  (armPwm,handPwm)
        delArmPwm=armPwm - self.arm.pwmVal
        delHandPwm=handPwm - self.hand.pwmVal
        startArmPwm=self.arm.pwmVal
        startHandPwm=self.hand.pwmVal

        if self.dbg:
            print 'delPWM:  arm, hand',delArmPwm, delHandPwm
        sgnArm=1
        if delArmPwm<0:
            sgnArm=-1
            delArmPwm*=-1

        sgnHand=1
        if delHandPwm<0:
            sgnHand=-1
            delHandPwm*=-1

        ntick=int(timeSec/self.tickSec)
        if ntick<1:
            ntick=1
        #print "ntick=",ntick,timeSec,self.tickSec
        accArm =2.*delArmPwm/ntick/ntick
        accHand=2.*delHandPwm/ntick/ntick
        #print 'Arm : delPwm=%.1f, accArm=%.3g sgn=%d' % (delArmPwm,accArm,sgnArm)
        #print 'Hand: delPwm=%.1f, accHand=%.2f sgn=%d' % (delHandPwm,accHand,sgnHand) 

        for i in range(ntick-1):
            k=i+1
            pwmA= startArmPwm +int (sgnArm  *accArm*k*k/2.)
            pwmH= startHandPwm+int (sgnHand *accHand*k*k/2.)
            #print "i=%d  pwmArm=%d pwmHand=%d"% (i,pwmA,pwmH)
            self.arm.setPwm( pwmA)
            self.hand.setPwm( pwmH)            
            time.sleep(self.tickSec)   

        self.arm.setPwm( armPwm)    
        self.hand.setPwm( handPwm)
        if self.dbg:    
            print "new status arm=%s, hand=%s" %(self.arm.status, self.hand.status)
        

    def fullStop(self):
        print "crawlerServos STOP & RELAX"
        self.hand.fullStop() # this one owns controller
