# god-class owning crawler body,SM,GUI,servos, mouse
# -------------------------

import Tkinter
import time
import threading
import sys
from crawlerBodyModel import CrawlingRobotBody
from crawlerStateMachine import CrawlingRobotSM

import math
from math import pi as PI

import sys
import os 
import pickle

# define path to other modules
sys.path.append(os.path.relpath("../servosUtil/"))
sys.path.append(os.path.relpath("../mouseUtil/"))
#print sys.path ;exit(0) # print path variable if you are in trouble


#--------------------------------------------
#------ define class ApplicationCrawlerReflex
#--------------------------------------------

class ApplicationCrawlerReflex:

    def updateGuiInfo(self):
        
        armBucket,handBucket = self.robotSM.getCurrentState()
        self.arm_bucket['text'] = 'bucket=%d [0,%d]' % (armBucket,self.robotSM.nArmStates-1)
        self.hand_bucket['text'] = 'bucket=%d [0,%d]' % (handBucket,self.robotSM.nHandStates-1)
        armAngle,handAngle = self.robotBody.getAngles()
        self.arm_angle['text'] = 'ang/deg=%.0f' % (armAngle/PI*180)
        self.hand_angle['text'] = 'ang/deg=%.0f' % ((handAngle)/PI*180)
        robPos = self.robotBody.getRobotPosition()
        self.body_pos['text'] = 'body pos=%.0f' % robPos[0]
        robRot = self.robotBody.getRotationAngle()
        self.body_rot['text'] = 'body rot/deg=%.0f' % (robRot/PI*180)
        if  self.useServos:
            #old (mou_rew,mou_pos)=self.mouse.getStepReward()
            (mou_rew,mou_pos,mou_dur)=self.mouse.getRestReward()
            self.mouse_rew['text'] = 'mou rew=%.0f' % mou_rew   
            self.mouse_pos['text'] = 'mou pos=%.0f' % mou_pos   
            self.arm_pwm['text'] = 'pwm=%d'% self.servos.arm.pwmVal
            self.hand_pwm['text'] = 'pwm=%d'% self.servos.hand.pwmVal
        else:
            self.mouse_rew['text'] = 'mechanics=OFF'

    def setupKeyapd(self):
        rowC=0; colC=0
        label1 = Tkinter.Label(self.win, text='one move')
        label1.grid(row=rowC, column=colC)

        #--- arm
        mid = Tkinter.Button(self.win,text="reset",command=(lambda: self.actMoveHandArm('reset')))
        mid.grid(row=rowC, column=colC+1, padx=10) 

        aR = Tkinter.Button(self.win,text="arm-up",command=(lambda: self.actMoveHandArm('arm-up')))
        aR.grid(row=rowC+1, column=colC, padx=10) 

        aL = Tkinter.Button(self.win,text="arm-down",command=(lambda: self.actMoveHandArm('arm-down')))
        aL.grid(row=rowC+2, column=colC, padx=10) 

        self.arm_bucket = Tkinter.Label(self.win, text='bucket=:X ')
        self.arm_bucket.grid(row=rowC+3, column=colC) 

        self.arm_angle = Tkinter.Label(self.win, text='ang/deg=:X ')
        self.arm_angle.grid(row=rowC+4, column=colC) 

        self.arm_pwm = Tkinter.Label(self.win, text='pwm=:X ')
        self.arm_pwm.grid(row=rowC+5, column=colC) 

        
        # --- hand
        colC+=1
        hU = Tkinter.Button(self.win,text="hand-up",command=(lambda: self.actMoveHandArm('hand-up')))
        hU.grid(row=rowC+1, column=colC, padx=10) 

        hD = Tkinter.Button(self.win,text="hand-down",command=(lambda: self.actMoveHandArm('hand-down')))
        hD.grid(row=rowC+2, column=colC, padx=10) 

        self.hand_bucket = Tkinter.Label(self.win, text='bucket=:Y ')
        self.hand_bucket.grid(row=rowC+3, column=colC) 

        self.hand_angle = Tkinter.Label(self.win, text='ang/deg=:X ')
        self.hand_angle.grid(row=rowC+4, column=colC) 

        self.hand_pwm = Tkinter.Label(self.win, text='pwm=:Y ')
        self.hand_pwm.grid(row=rowC+5, column=colC) 


        # --- virt body
        colC+=1
        label2 = Tkinter.Label(self.win, text='virt body')
        label2.grid(row=rowC, column=colC)

        self.virt_rew = Tkinter.Label(self.win, text='rew=:Y ')
        self.virt_rew.grid(row=rowC+1, column=colC) 

        self.body_pos = Tkinter.Label(self.win, text='position=:Y ')
        self.body_pos.grid(row=rowC+2, column=colC) 

        self.body_rot = Tkinter.Label(self.win, text='body rot/deg=:X ')
        self.body_rot.grid(row=rowC+3, column=colC) 

        Tkinter.Label(self.win, text='config: '+self.bodyConfigFile).grid(row=rowC+5, column=colC,columnspan=2)
    
        # --- phys body
        colC+=1
        label3 = Tkinter.Label(self.win, text='phys body')
        label3.grid(row=rowC, column=colC)

        self.mouse_rew = Tkinter.Label(self.win, text='mou rew=none ')
        self.mouse_rew.grid(row=rowC+1, column=colC) 

        self.mouse_pos = Tkinter.Label(self.win, text='mou pos=N/A')
        self.mouse_pos.grid(row=rowC+2, column=colC) 

        self.arm_chan = Tkinter.Label(self.win, text='arm_chan=N/A')
        self.arm_chan.grid(row=rowC+3, column=colC) 

        self.hand_chan = Tkinter.Label(self.win, text='hand_chan=N/A')
        self.hand_chan.grid(row=rowC+4, column=colC) 

        
    def __initGUI(self, win):
        ## Window ##
        self.win = win

        ## Initialize Frame ##
        win.grid()

        ## Exit Button ##
        self.exit_button = Tkinter.Button(win,text='Quit', command=self.actExit)
        self.exit_button.grid(row=0, column=9)


        ## Resume Button ##
        self.resume_button = Tkinter.Button(win,text='Resume', command=self.actResume)
        self.resume_button.grid(row=0, column=7)

       #
        self.var_once= Tkinter.IntVar()
        Tkinter.Checkbutton(self.win, text="once", variable=self.var_once).grid(row=1, column=7)
 
        ## Pause Button ##
        self.pause_button = Tkinter.Button(win,text='Pause', command=self.actPause)
        self.pause_button.grid(row=2, column=7)

        #
        self.policy_txt = Tkinter.Label(self.win, text="Policy ?");        
        self.policy_txt.grid(row=3, column=6, padx=10,columnspan=2) 



        ## Canvas2D ##
        self.canvas2D = Tkinter.Canvas(self.win, height=200, width=crawlerConfig['canvasLenght'])
        self.canvas2D.grid(row=6,columnspan=10)

        self.setupKeyapd()
        self.setupActionChoice()

       
    def setupActionChoice(self):

        col=6; row=0;
        self.agentType =  Tkinter.IntVar()
        self.agentType.set(0)
        
        self.tickTau_txt = Tkinter.Label(self.win, text="Walk ?");        
        self.tickTau_txt.grid(row=row, column=col, padx=10) 

        rad1=Tkinter.Radiobutton(self.win, text="forward", variable=self.agentType, value=1, command=self.actReflexAgent).grid(row=row+1, column=col, padx=10) 
    
        rad2=Tkinter.Radiobutton(self.win, text="backward", variable=self.agentType, value=2, command=self.actReflexAgent)
        rad2.grid(row=row+2, column=col, padx=10) 

        tickFreqSlider = Tkinter.Scale(self.win, from_=1, to=8, tickinterval=2, orient=Tkinter.HORIZONTAL,  command=self.actTickSpeed)
        tickFreqSlider.grid(row=row+5, column=col, padx=10) 
        tickFreqSlider.set(5)
        self.actTickSpeed(5)

        slide_txt = Tkinter.Label(self.win, text="<= speed");        
        slide_txt.grid(row=row+5, column=col+1, padx=10) 

    # --------------------------       
    def __init__(self, win, opts):
        
        self.dbg=opts.VERBOSE
        self.bodyConfigFile=opts.SETUP+opts.CONF_NAME
        execfile(self.bodyConfigFile) # load  physical specs of the crawler
        if self.dbg:
            print "Define mechanical crawler body using:",self.bodyConfigFile
            print crawlerConfig
        self.stepCount = 0
        self.forwPolicyName=opts.SETUP+crawlerConfig['reflexForwPolicy']
        self.backwPolicyName=opts.SETUP+crawlerConfig['reflexBackwPolicy']


        ## Init Gui
        self.__initGUI(win)

        # Init crawler
        ## Robot Servos - if activated##
  
        if not opts.MOCK_SERVOS:
            from crawlerServos import CrawlerServos
            from mouseDriver import MouseDriver 
            print 'Connecting to servos....'
            self.useServos=True
            self.servos=CrawlerServos(crawlerConfig, opts.SETUP, self.dbg)
            self.arm_chan['text']='arm_chan=%d'% self.servos.arm.myPin
            self.hand_chan['text']='hand_chan=%d'% self.servos.hand.myPin
            print 'Connecting to mouse....'
            pickleName=opts.SETUP+crawlerConfig['mouseConfig']
            print "Load mouse config from ",pickleName 
            if not os.path.isfile(pickleName) :
                print ' not existing input pickel name, do nothing ???'
                exit(1)
            mConf=pickle.load( open(pickleName, "r" ) )
            self.mouse=MouseDriver(mConf)
            self.mouse.start()  # start a thread for mouse 
        else:
            self.useServos=False
            print 'NOT connecting to hardware '

        if crawlerConfig['name'] == 'crawler5':
            self.robotBody = CrawlingRobotBody(self.canvas2D, crawlerConfig)
            self.robotSM = CrawlingRobotSM(self.robotBody, crawlerConfig)
        else:    
            raise "Unknown RobotType"

        self.actMoveHandArm('reset')

        self.updateGuiInfo()    
 
        # Start GUI
        self.running = False
        self.stopped = False
 
        self.thread = threading.Thread(target=self.actRun)
        self.thread.start()

    def actPause(self):
        #print 'actPause pressed'
        self.running = False

      
    def actResume(self):
        self.running = True
        self.thread = threading.Thread(target=self.actRun)
        self.thread.start()

    def actTickSpeed(self,freq):
        self.tickTime = 1./float(freq)
        self.tickTau_txt['text'] = 'Walk %.2f sec/step' %  self.tickTime

    def actMoveHandArm(self,action):
        if self.dbg:
            print '-----actMoveHandArm action=',action
        currState= self.robotSM.getCurrentState()
        actionsL =  self.robotSM.getPossibleActions(currState)
        #print "robJ:   possibleActions",actionsL
        isAllowed=action in actionsL        
        #print "robJ: currState rob=",currState, ' isAllowed=',isAllowed

        if isAllowed:
            nextState, reward, newPos=self.robotSM.doAction(action)
            self.virt_rew['text'] = 'virt rew=%.2g' % (reward)

        if action=='reset':
            par_reset_pos=80
            print 'reset action it is'
            self.robotBody.reset(par_reset_pos)  # it is virtual body
            if self.useServos:
                self.mouse.posReset(par_reset_pos)
            self.robotSM.reset()
             
        armAng,handAng=self.robotBody.getAngles()
        if self.dbg:
            print 'new set armAng=%.3f handAng=%.3f  (rad)'%(armAng,handAng)   

        if self.useServos:
           self.servos.setSoftly( armAng, handAng-armAng,self.tickTime)
        else:
            time.sleep(self.tickTime)
        self.updateGuiInfo()    


    def actExit(self):
        if  self.useServos:
            self.servos.fullStop()
            self.mouse.needToStop=True
            self.useServos=False # to prevent multiple stops  
            # to un-block mouse w/o input see this idea of using
            #http://stackoverflow.com/questions/21429369/read-file-with-timeout-in-python


        if self.running:
            self.running = False
            time.sleep(0.6)

        for i in range(5):
            if not self.stopped:
                time.sleep(0.1)
        try:
            self.win.destroy()
        except:
            pass
    
        sys.exit(0)

    def actStep(self,act='reset'):
        self.actMoveHandArm(act)

 
    def actRun(self):
        self.stepCount = 0 
        if self.agentType.get()==0:
            self.stopped = True
            #print "Can't run: agentType=",self.agentType.get()
            return
        for act in self.reflexActionPolicy:
            if not self.running:
                self.stopped = True
                return
            self.actStep(act)

    def actStart(self):
        #print 'in actStart'
        self.win.mainloop()


    def actReflexAgent(self):
        from itertools import cycle
        print 'set reflex agent type ',self.agentType.get()
        self.policy_txt['text'] = 'Policy: ?? '
        if self.agentType.get()==1:  
            policyName=self.forwPolicyName
        elif  self.agentType.get()==2: 
            policyName=self.backwPolicyName
        else:
            print 'XX no policy XX???'
            return
        print "Load walking policy from ",policyName 
        if not os.path.isfile(policyName) :
                print ' not existing input policy name, do nothing ???'
                return
        execfile(policyName)
        self.policy_txt['text'] = 'Policy: '+policyName
        if self.dbg:
            print "Loaded walking policy:",walkPolicy,' flag-once=',self.var_once.get()

        if  self.var_once.get():
            self.reflexActionPolicy = walkPolicy
        else:
            self.reflexActionPolicy = cycle(walkPolicy)
        print " remember to 'reset' crawler to define its state before running a policy"   
 
