# qlearningAgents.py
# ------------------
# Licensing Information:  You are free to use or extend these projects for 
# educational purposes provided that (1) you do not distribute or publish 
# solutions, (2) you retain this notice, and (3) you provide clear 
# attribution to UC Berkeley, including a link to 
# http://inst.eecs.berkeley.edu/~cs188/pacman/pacman.html
# 


import random,math
import sys,os
# define path to other modules
sys.path.append(os.path.relpath("../utils-cs188/"))
#print sys.path ;exit(0) # print path variable if you are in trouble

import util
from game import *
from learningAgents import ReinforcementAgent
from featureExtractors import *

class QLearningAgent(ReinforcementAgent):
    """
      Q-Learning Agent

      Functions you should fill in:
        - computeValueFromQValues
        - computeActionFromQValues
        - getQValue
        - getAction
        - update

      Instance variables you have access to
        - self.epsilon (exploration prob)
        - self.alpha (learning rate)
        - self.discount (discount rate)

      Functions you should use
        - self.getLegalActions(state)
          which returns legal actions for a state
    """
    def __init__(self, **args):
        "You can initialize Q-values here..."
        ReinforcementAgent.__init__(self, **args)
        "*** YOUR CODE HERE ***"
        self.qvalues = util.Counter() 
        
    def getQValue(self, state, action):
        """
          Returns Q(state,action)
          Should return 0.0 if we have never seen a state
          or the Q node value otherwise
        """
        "*** YOUR CODE HERE ***"
        return self.qvalues[(state,action)]


    def computeValueFromQValues(self, state):
        """
          Returns max_action Q(state,action)
          where the max is over legal actions.  Note that if
          there are no legal actions, which is the case at the
          terminal state, you should return a value of 0.0.
        """
        "*** YOUR CODE HERE ***"
        legalActions = self.getLegalActions(state)
        #print "1-legAct=",legalActions
        if len(legalActions)==0:
            return 0.0 
        valBest=-float('inf')
        for act in legalActions:
            val=self.getQValue(state,act)
            #print "  1-for act=",act," futu Qvale=",val
            if valBest<val:
                valBest=val
        return valBest        

    def computeActionFromQValues(self, state):
        """
          Compute the best action to take in a state.  Note that if there
          are no legal actions, which is the case at the terminal state,
          you should return None.
        """
        "*** YOUR CODE HERE ***"
        legalActions = self.getLegalActions(state)
        #print "2-legAct=",legalActions
        if len(legalActions)==0:
            return None 
        valBest=-float('inf')
        actBest=None
        for act in legalActions:
            val=self.getQValue(state,act)
            #print "  2-for act=",act," futu Qvale=",val
            if valBest>val:
                continue
            if valBest==val: # check for tie
                r = random.random()
                if r<0.5 :
                    continue
                #  found better action, remember it   
            valBest=val
            actBest=act
        return actBest        


    def getAction(self, state):
        """
          Compute the action to take in the current state.  With
          probability self.epsilon, we should take a random action and
          take the best policy action otherwise.  Note that if there are
          no legal actions, which is the case at the terminal state, you
          should choose None as the action.

          HINT: You might want to use util.flipCoin(prob)
          HINT: To pick randomly from a list, use random.choice(list)

           epsilon-greedy action selection
        """
        # Pick Action
        legalActions = self.getLegalActions(state)
        action = None
        "*** YOUR CODE HERE ***"
        if len(legalActions)==0:
            return action
        r = random.random()
        if r<self.epsilon: # pick action ar random
            action=random.choice(legalActions)
        else:
            action=self.computeActionFromQValues(state)
        #test
        #action='hand-down'
        #action='hand-up'
        #action='arm-up'
        #action='arm-down'
        #print "ql: chosen act=",action    
        return action
    

    def update(self, state, action, nextState, reward):
        """
          The parent class calls this to observe a
          state = action => nextState and reward transition.
          You should do your Q-Value update here

          NOTE: You should never call this function,
          it will be called on your behalf
        """
        "*** YOUR CODE HERE ***"
        #print "jan update state=",state," act=",action," nextSt=",nextState," rew=",reward

        newQvalue=reward+self.discount*self.computeValueFromQValues(nextState)
        #print " newQval=",newQvalue
        #print "oldQ=",self.qvalues[(state,action)]," st,act=",(state,action)
        self.qvalues[(state,action)]= (1-self.alpha)*self.qvalues[(state,action)] + self.alpha*newQvalue
        #print " --> updatedQ=",self.qvalues[(state,action)]


    def getPolicy(self, state):
        return self.computeActionFromQValues(state)

    def getValue(self, state):
        return self.computeValueFromQValues(state)

