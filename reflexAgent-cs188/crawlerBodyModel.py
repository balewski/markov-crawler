#!/usr/bin/python

# ----------
# Licensing Information:  You are free to use or extend these projects for 
# educational purposes provided that (1) you do not distribute or publish 
# solutions, (2) you retain this notice, and (3) you provide clear 
# attribution to UC Berkeley, including a link to 
# http://inst.eecs.berkeley.edu/~cs188/pacman/pacman.html
# 
# modified by Jan Balewski to controll physical crawler body (jan.balewski@gmail.com)
#
import math
from math import pi as PI
import time
import random


#--------------------------------------------
#------ define class CrawlingRobotBody
#------ variables arecontinous in radians or mm
#--------------------------------------------
class CrawlingRobotBody:

    def setAngles(self, armAngle, handAngle):
        """
            set the robot's arm and hand angles
            to the passed in values
        """
        self.armAngle = armAngle
        self.handAngle = handAngle

    def getAngles(self):
        """
            returns the pair of (armAngle, handAngle)
        """
        return self.armAngle, self.handAngle

    def getRobotPosition(self):
        """
            returns the (x,y) coordinates
            of the lower-left point of the
            robot
        """
        return self.robotPos

    def moveArm(self, newArmAngle):
        """
            move the robot arm to 'newArmAngle'
        """
        oldArmAngle = self.armAngle
        if newArmAngle > self.maxArmAngle:
            raise 'Crawling Robot: Arm Raised too high. Careful!'
        if newArmAngle < self.minArmAngle:
            raise 'Crawling Robot: Arm Raised too low. Careful!'
        disp = self.virtualDisplacement(self.armAngle, self.handAngle,
                                  newArmAngle, self.handAngle)

        curXPos = self.robotPos[0]
        self.robotPos = (curXPos+disp, self.robotPos[1])
        self.armAngle = newArmAngle

        # Position and Velocity Sign Post, needed for 100-step veraged speed
        self.positions.append(self.getRobotPosition()[0])
        if len(self.positions) > 100:
            self.positions.pop(0)

    def moveHand(self, newHandAngle):
        """
            move the robot hand to 'newArmAngle'
        """
        oldHandAngle = self.handAngle
        #print "xx old, newAngle=",oldHandAngle,newHandAngle
        if newHandAngle > self.maxHandAngle*0.95: 
            raise 'Crawling Robot: Hand Raised too high. Careful!'
        if newHandAngle < self.minHandAngle*1.05: 
            raise 'Crawling Robot: Hand Raised too low. Careful!'
        disp = self.virtualDisplacement(self.armAngle, self.handAngle, self.armAngle, newHandAngle)

        curXPos = self.robotPos[0]
        self.robotPos = (curXPos+disp, self.robotPos[1])
        self.handAngle = newHandAngle

        # Position and Velocity Sign Post
        self.positions.append(self.getRobotPosition()[0])
        if len(self.positions) > 100:
            self.positions.pop(0)
 
    def getMinAndMaxArmAngles(self):
        """
            get the lower- and upper- bound
            for the arm angles returns (min,max) pair
        """
        return self.minArmAngle, self.maxArmAngle

    def getMinAndMaxHandAngles(self):
        """
            get the lower- and upper- bound
            for the hand angles returns (min,max) pair
        """
        return self.minHandAngle, self.maxHandAngle

    def getRotationAngle(self):
        """
            get the current angle the
            robot body is rotated off the ground
        """
        armCos, armSin = self.__getCosAndSin(self.armAngle)
        handCos, handSin = self.__getCosAndSin(self.handAngle) #ff +self.armAngle
        x = self.armLength * armCos + self.handLength * handCos + self.robotWidth
        y = self.armLength * armSin + self.handLength * handSin + self.robotHeight
        if y < 0:
            return math.atan(-y/x)
        return 0.0

    def __getCosAndSin(self, angle):
        return math.cos(angle), math.sin(angle)

    def virtualDisplacement(self, oldArmDegree, oldHandDegree, armDegree, handDegree):
        #computes how far in x robot body will move, returns newX
        
        oldArmCos, oldArmSin = self.__getCosAndSin(oldArmDegree)
        armCos, armSin = self.__getCosAndSin(armDegree)
        oldHandCos, oldHandSin = self.__getCosAndSin(oldHandDegree)
        handCos, handSin = self.__getCosAndSin(handDegree)

        xOld = self.armLength * oldArmCos + self.handLength * oldHandCos + self.robotWidth
        yOld = self.armLength * oldArmSin + self.handLength * oldHandSin + self.robotHeight

        x = self.armLength * armCos + self.handLength * handCos + self.robotWidth
        y = self.armLength * armSin + self.handLength * handSin + self.robotHeight

        if y < 0:
            if yOld <= 0:
                return math.sqrt(xOld*xOld + yOld*yOld) - math.sqrt(x*x + y*y)
            return (xOld - yOld*(x-xOld) / (y - yOld)) - math.sqrt(x*x + y*y)
        else:
            if yOld  >= 0:
                return 0.0
            return -(x - y * (xOld-x)/(yOld-y)) + math.sqrt(xOld*xOld + yOld*yOld)

        raise 'Never Should See This!'

    #...... methods draw robot on X-Y canvas + updates metrics at the bottom of it
    def draw(self, stepCount, stepDelay):
        x1, y1 = self.getRobotPosition()
        x1 = x1 % self.totWidth

        ## Check Lower Still on the ground
        if y1 != self.groundY:
            raise 'Flying Robot!!'

        rotationAngle = self.getRotationAngle()
        cosRot, sinRot = self.__getCosAndSin(rotationAngle)

        x2 = x1 + self.robotWidth * cosRot
        y2 = y1 - self.robotWidth * sinRot

        x3 = x1 - self.robotHeight * sinRot
        y3 = y1 - self.robotHeight * cosRot

        x4 = x3 + cosRot*self.robotWidth
        y4 = y3 - sinRot*self.robotWidth

        self.canvas.coords(self.robotBody,x1,y1,x2,y2,x4,y4,x3,y3)

        armCos, armSin = self.__getCosAndSin(rotationAngle+self.armAngle)
        xArm = x4 + self.armLength * armCos
        yArm = y4 - self.armLength * armSin

        self.canvas.coords(self.robotArm,x4,y4,xArm,yArm)

        handCos, handSin = self.__getCosAndSin(self.handAngle+rotationAngle) #ff +self.armAngle
        xHand = xArm + self.handLength * handCos
        yHand = yArm - self.handLength * handSin

        self.canvas.coords(self.robotHand,xArm,yArm,xHand,yHand)

        # Position and Velocity Sign Post
        steps = (stepCount - self.lastStep)
        if steps==0:return
        pos = self.positions[-1]
        velocity = pos - self.positions[-2]
        vel2 = (pos - self.positions[0]) / len(self.positions)
        self.velAvg = .9 * self.velAvg + .1 * vel2
        velMsg = '100-step Avg Velocity: %.2f' % self.velAvg
        velocityMsg = 'Velocity: %.2f' % velocity
        positionMsg = 'Position: %2.f' % pos
        stepMsg = 'Step: %d' % stepCount
        if 'vel_msg' in dir(self):
            self.canvas.delete(self.vel_msg)
            self.canvas.delete(self.pos_msg)
            self.canvas.delete(self.step_msg)
            self.canvas.delete(self.velavg_msg)
        self.velavg_msg = self.canvas.create_text(650,190,text=velMsg)
        self.vel_msg = self.canvas.create_text(450,190,text=velocityMsg)
        self.pos_msg = self.canvas.create_text(280,190,text=positionMsg)
        self.step_msg = self.canvas.create_text(50,190,text=stepMsg)
        self.lastStep = stepCount

    def reset(self,x=0):
        """
         Resets the body to the initial state
        """
        self.robotPos = (x, self.groundY)


    def __init__(self, canvas, crawlerConfig):

        ## Canvas ##
        self.canvas = canvas
        self.velAvg = 0
        self.lastStep = 0

        ## Arm and Hand Degrees ##
        self.armAngle = self.oldArmDegree = 0.0
        self.handAngle = self.oldHandDegree = 0.0

        # sets the range of arm & hand movement
        self.maxArmAngle = crawlerConfig['armHighAngle']
        self.minArmAngle = crawlerConfig['armLowAngle']

        self.maxHandAngle = crawlerConfig['handHighAngle']
        self.minHandAngle = crawlerConfig['handLowAngle']

        ## Draw Ground ##
        self.totWidth = canvas.winfo_reqwidth()
        self.totHeight = canvas.winfo_reqheight()
        self.groundHeight = 30
        self.groundY = self.totHeight - self.groundHeight

        self.ground = canvas.create_rectangle(0,
            self.groundY,self.totWidth,self.totHeight, fill='#fb0')

        ## Robot Body ##
        self.robotWidth = crawlerConfig['bodyWidth']
        self.robotHeight = crawlerConfig['bodyHeight']
        self.robotBody = canvas.create_polygon(0,0,0,0,0,0,0,0, fill='blue')

        ## Robot Arm ##
        self.armLength = crawlerConfig['armLength']
        self.robotArm = canvas.create_line(0,0,0,0,fill='orange',width=5)

        ## Robot Hand ##
        self.handLength = crawlerConfig['handLength']
        self.robotHand = canvas.create_line(0,0,0,0,fill='red',width=3)
        self.reset()

 


           

