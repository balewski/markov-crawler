#!/usr/bin/python
"""
Example of reflex agent driving 2 servos using a varying in time functions.
Walking is smooth, servo angles chaneg with sin/cos modulation.

Main must be executed as sudo or gksudo

"""

import sys
import os
import time
import pickle
import math

# define path to other modules
sys.path.append(os.path.relpath("../servosUtil/"))
#print sys.path ;exit(0) # print path variable if you are in trouble

from servoDriver import ServoDriver 
import sys

#===============================
#=======    MAIN Program
#===============================

print 'Number of arguments:', len(sys.argv), 'arguments.'
print 'Argument List:', str(sys.argv)

# define parameters below
arm_hand_phase=80 #deg , use80 for forward and -100 for backward walk
if  len(sys.argv)==2:
    arm_hand_phase=float(sys.argv[1])
print "use arm-hand phas=%d deg"%arm_hand_phase
walkTime=10. #  (sec) , total duration of walking
stepDelay=0.05 # (seconds), between micro-steps
rotation_omega=250  # deg/second
# define  mean angle and amplitude of oscillations, units: deg
arm_phi0=-10
arm_ampl=30
hand_phi0=-100
hand_ampl=50

handConfName="../setup/hand.servo.conf"
armConfName ="../setup/arm.servo.conf"
dbg=0  # debugging switch , set to 1 to see more printouts

# open pickles for hand and arm and initialize two instances of ServoDriver
handConf=pickle.load( open(handConfName, "r" ) )
print 'loaded hand-servo conf',handConfName
if dbg:
    print handConf

hand = ServoDriver(dbg)
hand.setupController()
hand.config(handConf)

armConf=pickle.load( open(armConfName, "r" ) )
print 'loaded arm-servo conf ',armConfName
if dbg:
    print armConf
    
arm = ServoDriver(dbg)
arm.linkController(hand.pwmDriver) # use commin PWM driver
arm.config(armConf)

"""
position arm,hand  in some neutral angle, 
must execute sequential because at cold-start the servos positions are unknown
and they can draw a lot of current
"""

stateReset=(20,-15) # (arm,hand) angles in deg  at reset position
arm.setAngleDeg(stateReset[0])
time.sleep(0.5) # this delay desynchronizes hand from arm, keep it 
hand.setAngleDeg(stateReset[1])
time.sleep(0.5)

print "Arm and Hand ready, start walking"

while walkTime >0 :
    
    x=rotation_omega*walkTime
    xHand=math.radians(x+arm_hand_phase)
    xArm=math.radians(x)
    hand_phi= hand_phi0+math.sin(xHand)*hand_ampl
    arm_phi= arm_phi0+math.sin(xArm)*hand_ampl
    arm.setAngleDeg(arm_phi)
    hand.setAngleDeg(hand_phi-arm_phi) # no hand angle is vs. body
    time.sleep(stepDelay)
    walkTime=walkTime - stepDelay
    
hand.fullStop()  # must be executed to free servos

