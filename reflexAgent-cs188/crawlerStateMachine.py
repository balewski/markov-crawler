#!/usr/bin/python

#  only one class  CrawlingRobotEnvironment
# ----------
# Licensing Information:  You are free to use or extend these projects for 
# educational purposes provided that (1) you do not distribute or publish 
# solutions, (2) you retain this notice, and (3) you provide clear 
# attribution to UC Berkeley, including a link to 
# http://inst.eecs.berkeley.edu/~cs188/pacman/pacman.html
# 
# extended by Jan Balewski to display q-values and state values (jan.balewski@gmail.com)
#
import math
from math import pi as PI
import time
from stateMachine import StateMachine 
import random

#--------------------------------------------
#------ define class  CrawlingRobotEnvironment(environment.Environment)
#--------------------------------------------

class CrawlingRobotSM(StateMachine):

    def __init__(self, crawlingBodyModel,crawlerConfig):

        self.crawlingBodyModel = crawlingBodyModel

        # The state is of the form (armAngle, handAngle)
        # where the angles are bucket numbers, not actual
        # degree measurements
        self.state = None
        
        self.nArmStates = crawlerConfig['armNbucket']
        self.nHandStates = crawlerConfig['handNbucket']
        
        # jan: secondary variables used for displaying q-values & state values
        self.grid_width=self.nArmStates
        self.grid_height=self.nHandStates

        # create a list of arm buckets and hand buckets to
        # discretize the state space
        minArmAngle,maxArmAngle = self.crawlingBodyModel.getMinAndMaxArmAngles()
        minHandAngle,maxHandAngle = self.crawlingBodyModel.getMinAndMaxHandAngles()
        armIncrement = (maxArmAngle - minArmAngle) / (self.nArmStates-1)
        handIncrement = (maxHandAngle - minHandAngle) / (self.nHandStates-1)
        self.armBuckets = [minArmAngle+(armIncrement*i) \
           for i in range(self.nArmStates)]
        self.handBuckets = [minHandAngle+(handIncrement*i) \
         for i in range(self.nHandStates)]

        # Reset
        self.reset()

    def getCurrentState(self):
        """
          Return the current state
          of the crawling robot
        """
        #print  "X cur st=",self.state
        return self.state

    def getPossibleStates(self):
        """
        returns tuple with all possible states
        state convention: (y,x)? = (hand_i,arm_j)
        """
        stateT=()
        for y in range(self.grid_height):
            for x in range(self.grid_width):
                stateT+=((x,y),)  # xy, or y,x make no difference, the last 2 columns are not used
        return stateT
 

    def getPossibleActions(self, state):
        """
          Returns possible actions
          for the states in the
          current state
        """

        actions = list()

        currArmBucket,currHandBucket = state
        if currArmBucket > 0: actions.append('arm-down')
        if currArmBucket < self.nArmStates-1: actions.append('arm-up')
        if currHandBucket > 0: actions.append('hand-down')
        if currHandBucket < self.nHandStates-1: actions.append('hand-up')

        return actions

    def doAction(self, action):
        """
          Perform the action and update the current state of the SM
          Returns:
            nextState, reward, position
        """
        nextState, reward =  None, None

        oldX,oldY = self.crawlingBodyModel.getRobotPosition()

        armBucket,handBucket = self.state
        #j armAngle,handAngle = self.crawlingBodyModel.getAngles()
        #print 'doAction=',action, ' prior-bucket state=',self.state,' physAngle arm=%.0d, hand=%0d deg'%(self.armBuckets[armBucket]/PI*180,self.handBuckets[handBucket]/PI*180)

        if action == 'arm-up':
            newArmAngle = self.armBuckets[armBucket+1]
            self.crawlingBodyModel.moveArm(newArmAngle)
            nextState = (armBucket+1,handBucket)
        elif action == 'arm-down':
            newArmAngle = self.armBuckets[armBucket-1]
            self.crawlingBodyModel.moveArm(newArmAngle)
            nextState = (armBucket-1,handBucket)
        elif action == 'hand-up':
            newHandAngle = self.handBuckets[handBucket+1]
            self.crawlingBodyModel.moveHand(newHandAngle)
            nextState = (armBucket,handBucket+1)
        elif action == 'hand-down':
            newHandAngle = self.handBuckets[handBucket-1]
            self.crawlingBodyModel.moveHand(newHandAngle)
            nextState = (armBucket,handBucket-1)
        else:
            raise ' Illegal action=%s in SM-doAction'% action 

        newX,newY = self.crawlingBodyModel.getRobotPosition()

        # a simple reward function
        reward = newX - oldX
        #print "R: st-->nxSt", self.state,nextState," rew:",reward
        self.state = nextState
        #x armBucket,handBucket = self.state #for prtinting
        #x print ' post doAction reward=%.1f'%reward,' bucket state=',self.state,' physAngle arm=%.0d, hand=%0d deg'%(self.armBuckets[armBucket]/PI*180,self.handBuckets[handBucket]/PI*180)
        return nextState, reward, newX


    def reset(self):
        """
         Resets the SM to the initial state & match the body
        """
        armState = self.nArmStates-2
        handState = self.nHandStates-1
        self.state = armState,handState
        #print 'resetSM start-bucket state=',self.state,' physAngle arm=%0d, hand=%0d deg'%(self.armBuckets[armState]/PI*180,self.handBuckets[handState]/PI*180)
        self.crawlingBodyModel.setAngles(self.armBuckets[armState],self.handBuckets[handState])
        self.crawlingBodyModel.positions = [20,self.crawlingBodyModel.getRobotPosition()[0]]
        

